    package Chess;

    import java.awt.*;
    import java.awt.image.BufferedImage;
    import java.io.*;
    import javax.swing.*;
    import java.util.HashMap;
    import java.util.Map;
    import java.util.Scanner;


   class PTuple{
        int left;
        int right;
    PTuple(int x, int y){
        left = x;
        right = y;
        };

    public int getValues(boolean Right){
      if (Right){
         return right;
      } else {
         return left;
      }
        }
    

    boolean isEqualTo(PTuple T){
        if (T == null){
            return false;
        };
        if ((this.getValues(true) == T.getValues(true)) && (this.getValues(false) == T.getValues(false))){
            return true;
        } else {
            return false;
        }
    };
    int toInt(){
        return ((this.getValues(false)*10) + this.getValues(true));
    };
    };
    


   abstract class Pieces{
        boolean White;
        boolean state;
        int verMove;
        int horiMove;
        int yPos;
        int xPos;
    public Pieces(int x,int y,boolean white){
        xPos = x;
        yPos = y;
        White = white;
    };
    boolean Move(Board board,int x,int y){
       this.xPos = x;
       this.yPos = y;
       return true;

    };
    boolean Move(int x,int y){
        this.xPos = x;
        this.yPos = y;
        return true;

     };

        boolean doubleMoved(){
        return false;
    };

    boolean didNotMove(){
        return false;
    };

    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };

    Pieces returnThis(){
        return this;
    };
    abstract void Die();
    
    abstract String returnType();

    boolean isWhite(){
        return White;
    };
    boolean isEqualTo(Pieces piece){
        if (piece == null){
            return false;
        };

        if (this == piece){
            return true;
        } else {
            return false;
        }
    };
};


   class Checker{
    Checker(){

    };

    Pieces returnPiece(Board board,int xPos,int yPos){
      return board.findPiece(xPos , yPos);
    };

    int getPos(Pieces Piece){
        return Piece.returnPos();
    };

    Pieces getPiece(Pieces Piece){
        return Piece.returnThis();
    };

   };


//Stores the Pieces and their positions
    class Board{
    boolean game = true;
    HashMap<Integer,HashMap<Integer,Pieces>> mapMap = new HashMap<Integer,HashMap<Integer,Pieces>>();
    HashMap<Integer,Pieces> PieceMap = new HashMap<Integer,Pieces>();
    HashMap<Integer,Pieces> PieceMap2 = new HashMap<Integer,Pieces>();
    HashMap<Integer,Pieces> PieceMap3 = new HashMap<Integer,Pieces>();
    HashMap<Integer,Pieces> PieceMap4 = new HashMap<Integer,Pieces>();
    HashMap<Integer,Pieces> PieceMap5 = new HashMap<Integer,Pieces>();
    HashMap<Integer,Pieces> PieceMap6 = new HashMap<Integer,Pieces>();  
    HashMap<Integer,Pieces> PieceMap7 = new HashMap<Integer,Pieces>();  
    HashMap<Integer,Pieces> PieceMap8 = new HashMap<Integer,Pieces>();
    
    Pieces[] W_pawn = new Pieces[9];
    Pieces[] B_pawn = new Pieces[9];
    Pieces[] Placeholders = new Pieces[9];
    Checker Check = new Checker();


    Board(){
    };

    Pieces findPiece(int x,int y){
        Pieces Piece = (Pieces) mapMap.get(y).get(x);
       return Piece;
    };

    void setBoard(Board board){
        //Making the 2D array
        mapMap.put(1,PieceMap);
        mapMap.put(2,PieceMap2);
        mapMap.put(3,PieceMap3);    
        mapMap.put(4,PieceMap4);  
        mapMap.put(5,PieceMap5);   
        mapMap.put(6,PieceMap6); 
        mapMap.put(7,PieceMap7);  
        mapMap.put(8,PieceMap8);   
        
    for(int i=1 ; i!=9 ; i++){
        
        Pawn W_Pawn = new Pawn(i,2,true);
         W_pawn[i] = W_Pawn;
        board.recordPos(W_pawn[i]);
    };
    for(int i=1;i!=9;i++){
        Pawn B_Pawn = new Pawn(i,7,false);
        B_pawn[i] = B_Pawn;
        board.recordPos(B_pawn[i]);
    };
    for(int i=1;i!=9;i++){
        Placeholder Placeholder = new Placeholder(0,i,false);
        Placeholders[i] = Placeholder;
        board.recordPos(Placeholders[i]);
    };

    Rook W_Rook1 = new Rook(1,1,true);
    board.recordPos(W_Rook1);
    Rook W_Rook2 = new Rook(8,1,true);
    board.recordPos(W_Rook2);
    Rook B_Rook1 = new Rook(1,8,false);
    board.recordPos(B_Rook1);
    Rook B_Rook2 = new Rook(8,8,false);
    board.recordPos(B_Rook2);
    Horse W_Horse1 = new Horse(2,1,true);
    board.recordPos(W_Horse1);
    Horse W_Horse2 = new Horse(7,1,true);
    board.recordPos(W_Horse2);
    Horse B_Horse1 = new Horse(2,8,false);
    board.recordPos(B_Horse1);
    Horse B_Horse2 = new Horse(7,8,false);
    board.recordPos(B_Horse2);
    Bishop W_Bishop1 = new Bishop(3,1,true);
    board.recordPos(W_Bishop1); 
    Bishop W_Bishop2 = new Bishop(6,1,true);
    board.recordPos(W_Bishop2); 
    Bishop B_Bishop1 = new Bishop(3,8,false);
    board.recordPos(B_Bishop1); 
    Bishop B_Bishop2 = new Bishop(6,8,false);
    board.recordPos(B_Bishop2);
    Queen W_Queen = new Queen(4,1,true);
    board.recordPos(W_Queen);
    Queen B_Queen = new Queen(4,8,false);
    board.recordPos(B_Queen);
    King W_King = new King(5,1,true);
    board.recordPos(W_King);
    King B_King = new King(5,8,false);
    board.recordPos(B_King);

    };


    void kill(Pieces Piece){
        Piece.Die();
        mapMap.get(Piece.xPos).remove(Piece);

    };
    
    void remove(Pieces Piece) {
         mapMap.get(Piece.xPos).remove(); 
    };
    void recordPos(Pieces Piece){
    mapMap.get(Piece.yPos).put(Piece.xPos, Piece);

    };


};


    public class Chess{
    public static void main(String arg[]){
    Board board = new Board();


    if ((arg.length > 0) && (arg[0]== "-graphics")){
    


    //1. Create the frame.
    JFrame frame = new JFrame("Chess Demo",null);

    //2. Optional: What happens when the frame closes?
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //3. Create components and put them in the frame.
    ImageIcon Board = new ImageIcon("Chessboard.png");
    ImageIcon Rectangle = new ImageIcon("GrayRectangle.png");
    JLabel Label1 = new JLabel(Board,JLabel.CENTER);                             
    JLabel Label2 = new JLabel(Rectangle,JLabel.CENTER);


    //4. Size the frame.
    frame.pack();

    //5. Show it.
    frame.setVisible(true);
    } else {
        Checker chk = new Checker();
        Scanner scan = new Scanner(System.in);
        board.setBoard(board);
        boolean result;
        boolean whiteIsPlaying = false;
        int turn = 0;
        int tempY = 1;
        String printable;
        while (board.game){
            result = false;
            turn++;
            if (whiteIsPlaying == true){
                whiteIsPlaying = false;
            } else {
                whiteIsPlaying = true;
            };
            System.out.println("  12345678");
            for (int y = 1; y!=9 ; y++){
                StringBuilder sb = new StringBuilder();
                for (int x = 1; x!=9 ; x++){
                   if (board.findPiece(x,9 - y) == null){
                        sb.append(" ");
                        tempY = 9 - y;                  
                   } else {
                       String Type = new String();
                        Type = board.findPiece(x ,9 - y).returnType();
                        sb.append(Type.substring(0,1));
                        tempY = 9 - y;
                    };
                };
                System.out.println(tempY + " " + sb.toString());
            };
        while (result == false){
            System.out.println("Choose a piece by its position. Format:number,number");
            String I_Piece = scan.next();
            Pieces selection = board.findPiece(Integer.parseInt(I_Piece.substring(0,1)),Integer.parseInt(I_Piece.substring(2,3)));
            if (selection == null) {
                System.out.println("There are no pieces.");
            } else {
            System.out.println("Now choose a position to move it. Selected Piece-" + selection.returnType());
            String I_Pos = scan.next();
            if (whiteIsPlaying && selection.isWhite()){
                board.remove(selection);
                result = selection.Move(board,Integer.parseInt(I_Pos.substring(0,1)),Integer.parseInt(I_Pos.substring(2,3)));
                board.recordPos(selection);
            } else {
            if (!whiteIsPlaying && !selection.isWhite()){
                board.remove(selection);
                result = selection.Move(board,Integer.parseInt(I_Pos.substring(0,1)),Integer.parseInt(I_Pos.substring(2,3)));
                board.recordPos(selection);
            } else {
                System.out.println("That move is invalid.");

            };
            };
        };
        };
        };

        
    };
    };
};


    class Pawn extends Pieces{
        String type = "Pawn";
        boolean doubleMove = false;
        Checker checker = new Checker();





    public Pawn(int x,int y,boolean white){
       super(x,y,white);
    };

    boolean doubleMoved(){
        return doubleMove;
    };

    boolean Move(Board board,int x,int y){
     if ((White && y - yPos == 1) || (!White && yPos - y == 1)){
        yPos = y;
        xPos = x;
        return true;
     } else {
     if ((checker.returnPiece(board,x,(y + 1)).returnType() == "Pawn") && checker.returnPiece(board,x,(y + 1)).doubleMoved()){
            en_Passant(x , y,board);
        } else {
            System.out.println("That move is invalid.");
            return false;
        };
    };
    return false;
    };

    boolean en_Passant(int x, int y,Board board){
        if ((checker.returnPiece(board,xPos,(yPos + 1)).returnType() == "Pawn") && checker.returnPiece(board,xPos,(yPos + 1)).doubleMoved()){
            board.kill((checker.returnPiece(board,xPos,(yPos + 1))));
            xPos = x;
            yPos = y;
            return true;   
        };
        return false;
    };

    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };

    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };

    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };
};

    class Bishop extends Pieces{
        String type = "Bishop";
    public Bishop(int x,int y,boolean white){

       super(x,y,white);
    };

    boolean Move(int x, int y){
    if (White && (xPos - x == yPos - y) || (!White && (x - xPos == y - yPos))){
    xPos = x;
    yPos = y;
    return true;
    } else {
        System.out.println("That move is invalid.");
    };
    return false;
    };
    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };
    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };
    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };
};
    
    class Rook extends Pieces{
        Checker checker = new Checker();
        boolean neverMoved = true;
        String type = "Rook";

    public Rook(int x,int y,boolean white){
       super(x,y,white);
    };
    boolean Move(Board board,int x, int y){
    if (x != xPos){
        for (int xP = xPos;xP<x;xP++){
            Pieces result = checker.returnPiece(board,xP,yPos);
            if (result != null){
                System.out.println("There is a piece in the way.");
                return false;
            };
        };
        xPos = x;
        return true; 
    } else {   
        for (int yP = yPos;yP<y;yP++){
            Pieces result = checker.returnPiece(board,xPos,yP);
            if (result != null){
                System.out.println("There is a piece in the way.");
                return false;
            };
        }; 
    };
    yPos = y;
    return true;
    };


    boolean didNotMove(){
        return neverMoved;
    };
    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };

    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };
    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };
};
    class King extends Pieces{
    boolean neverMoved = true;
    Checker checker = new Checker();
    String type = "King";
    public King(int x,int y,boolean white){

       super(x,y,white);
    };

    boolean Move(Board board,int x,int y){
    if((Math.abs(xPos - x) <= 1) && (Math.abs(yPos - y) <= 1)){
        xPos = x;
        yPos = y;
        return true;
    } else {
    boolean noPieces = true;
        for(int pos = xPos;pos < x;pos++){
            Pieces result = checker.returnPiece(board,yPos,pos);
            if (result == null){
                noPieces = true;
            } else {
                System.out.println("There is a piece in the way.");
                return false;
            };
        };
        if (noPieces && (checker.returnPiece(board,yPos, x + 1).returnType() == "Rook") && checker.returnPiece(board,yPos, x + 1).didNotMove()){
            checker.returnPiece(board,yPos, x + 1).Move(board,y, x - 1);
            xPos = x;
            yPos = y;
            return true;
        };
    };
    return false;
    };

    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };

    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };
    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };
};

    class Queen extends Pieces{
        String type = "Queen";
    public Queen(int x,int y,boolean white){

       super(x,y,white);
    };

    boolean Move(int x, int y){
    if((x == xPos) && (y != yPos) || (x != xPos && y == yPos)){
        xPos = x;
        yPos = y;
        return true;
    } else {
        if(Math.abs(x - xPos) == Math.abs(y - yPos)){
            xPos = x;
            yPos = y;
            return true;
        } else {
            System.out.println("That move is invalid.");
        };
    };
    return false;
    };
    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };

    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };
    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };
};

    class Horse extends Pieces{
        String type = "Horse";
    public Horse(int x,int y,boolean white){

       super(x,y,white);
    };
    boolean Move(Board board,int x, int y){
        if ((Math.abs(xPos - x) == 1 && Math.abs(yPos - y) == 2) || (Math.abs(xPos - x) == 2 && Math.abs(yPos - y) == 1)){
            xPos = x;
            yPos = y;
            return true;
        } else {
            System.out.println("That move is invalid");
        };
    return false;
    };
    void Die(){
    state = false;
    System.out.println("This method is not supported. This is a Placeholder for method \"Die\" ");
    };

    int returnPos(){
    PTuple Returnable = new PTuple(xPos , yPos);
    return Returnable.toInt();
    };
    String returnType(){
        String temp = this.type;
        return temp;
    };
    boolean isWhite(){
        return White;
    };

    };
    class Placeholder extends Pieces{
        public Placeholder(int x,int y,boolean white) {
            super(x,y,white);
        };
        String returnType() {
            return "Placeholder";
        }
        void Die() {
            
        };
    }



